<?php
/**
 * @file
 * Field Incomplete - Provides a block displaying of what's currently incomplete on an entity - registry.
 */

/**
 * Implements hook_block_info().
 */
function _fc_incomplete_block_info() {
  $blocks = array();
  foreach (fc_entity_types() as $entity_type => $entity_info) {
    $blocks[$entity_type] = array(
      'info' => t('Field Incomplete: @type', array('@type' => $entity_info['label'])),
      'cache' => DRUPAL_NO_CACHE,
    );
  }
  return $blocks;
}

/**
 * Implements hook_theme().
 */
function _fc_incomplete_theme() {
  return array(
    'fc_incomplete_header' => array(
      'variables' => array('complete' => FALSE, 'name' => '', 'field_name' => '', 'entity_type' => '', 'entity' => NULL, 'bundle' => '', 'depth' => 0),
      'template' => 'fc-incomplete-header'
    ),
    'fc_incomplete_complete' => array(
      'variables' => array('complete' => FALSE),
      'file' => 'fc_incomplete.themes.inc',
    ),
    'fc_incomplete_entity_name' => array(
      'variables' => array('entity_type' => '', 'depth' => 0),
      'file' => 'fc_incomplete.themes.inc',
    ),
  );
}

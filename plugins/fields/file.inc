<?php
/**
 * @file
 * Field Complete - Provides field-based completeness for any entity - file field plugin.
 */

$plugin = array(
  'title' => t('FC: File field plugin'),

  'completeness check' => 'fc_file_field_is_complete',
  'incomplete process' => 'fc_file_incomplete_process',

  'field form' => array(
    'fc_check_fields' => array(
      '#type' => 'checkbox',
      '#title' => t('Check fields in the file entity (if any) for completeness'),
      '#description' => t('If checked, any fields attached to this file will also be checked for completeness.'),
    ),
  ),
);

/**
 * Use the default 'empty' hook to see if each entry has content
 */
function fc_file_field_is_complete($items, $instance, $field, &$completed = array()) {
  $module = $field['module'];
  $check_fields = !empty($instance['settings']['fc']['fc_check_fields']);

  foreach ($items as $delta => $item) {
    // Apply the initial test to see if this is a uploaded file
    $completed[$delta] = !module_invoke($module, 'field_is_empty', $item, $field);
    if ($completed[$delta] && $check_fields && !empty($item['fc'])) {
      // If it also has completeness check it's fields
      $completed[$delta] = $item['fc']->complete;
    }
  }
}

function fc_file_incomplete_process($parent, $function, $cardinality, $items, $instance, $field) {
  $incomplete = new fcIncomplete($instance['field_name'], $parent);
  $incomplete->complete = $cardinality($function, $items, $instance, $field);
  return $incomplete;
}
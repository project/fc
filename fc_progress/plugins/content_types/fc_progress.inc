<?php

/**
 * @file
 * Plugin to handle field completeness.
 */

/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */
$plugin = array(
  'title' => t('Field Complete Progress'),
  'defaults' => array(),
  'content type' => 'fc_progress_content_type_content_type',
  'content types' => 'fc_progress_content_type_content_types',
  'render callback' => 'fc_progress_content_type_render',
);

/**
 * Get the full definition of a given subtype.
 */
function fc_progress_content_type_content_type($subtype) {
  $types = fc_progress_content_type_content_types();
  if (isset($types[$subtype])) {
    return $types[$subtype];
  }
}

/**
 * Return all fc progress content types available
 */
function fc_progress_content_type_content_types() {
  $types = &drupal_static(__FUNCTION__, array());
  if (!empty($types)) {
    return $types;
  }

  $entity_info = entity_get_info();
  foreach ($entity_info as $entity_type => $info) {
    $types[$entity_type] = array(
      'category' => t('Field Complete'),
      'title' => t('Field Complete Progress: @label', array('@label' => $info['label'])),
      'required context' => new ctools_context_required($info['label'], $entity_type),
      'description' => t('Display the completeness of an entity.'),
      'edit form' => 'fc_progress_content_type_options',
    );
  }

  return $types;
}

/**
 * Render the content type.
 */
function fc_progress_content_type_render($subtype, $conf, $panel_args, $context) {
  if (empty($context) || empty($context->data)) {
    return;
  }

  // Get things ready.
  $entity_type = $subtype;
  $entity_info = entity_get_info($entity_type);
  $entity = $context->data;

  $block = new stdClass();
  $block->module = 'fc_progrss';
  $block->title = t('@title Progress', array('@title' => entity_label($entity_type, $entity)));
  $block->content = array(
    'bar' => array(
      '#theme' => 'fc_progress_bar',
      '#entity_type' => $entity_type,
      '#entity' => $entity,
    ),
    '#attached' => array(
      'css' => array(
        drupal_get_path('module', 'fc_progress') . '/fc_progress.css',
      ),
    ),
  );

  if (!empty($conf['show_next'])) {
    $block->content['next'] = array(
      '#theme' => 'fc_progress_next',
      '#entity_type' => $entity_type,
      '#entity' => $entity,
    );
  }

  $block->delta = $subtype;
  return $block;
}

/**
 * Returns an edit form for custom type settings.
 */
function fc_progress_content_type_options($form, &$form_state) {
  $form['show_next'] = array(
    '#title' => t('Show Next Element Message'),
    '#description' => t('Show a message like "Completing field name will raise completeness to 90%"'),
    '#type' => 'checkbox',
    '#default_value' => !empty($form_state['conf']['show_next']),
  );

  return $form;
}

function fc_progress_content_type_options_submit($form, &$form_state) {
  $form_state['conf']['show_next'] = $form_state['values']['show_next'];
}

/**
 * Returns an admin title for the type.
 */
function fc_progress_content_type_admin_title($subtype, $conf, $context) {
  return t('"@s" Completeness Progress', array('@s' => $context->identifier));
}

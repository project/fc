<?php
/**
 * @file
 * Field Complete Progress - Provides blocks to display a progress bar of Field completion - registry.
 */

/**
 * Implements hook_block_info().
 */
function _fc_progress_block_info() {
  $blocks = array();
  foreach (fc_entity_types() as $entity_type => $entity_info) {
    $blocks[$entity_type] = array(
      'info' => t('Field Complete Progress: @type', array('@type' => $entity_info['label'])),
      'cache' => DRUPAL_NO_CACHE,
    );
  }
  return $blocks;
}

/**
 * Implements hook_theme().
 */
function _fc_progress_theme() {
  return array(
    'fc_progress_bar' => array(
      'variables' => array('entity_type' => '', 'entity' => NULL),
      'template' => 'fc-progress-bar',
      'file' => 'fc_progress.blocks.inc',
    ),
    'fc_progress_next' => array(
      'variables' => array('entity_type' => '', 'entity' => NULL, 'field_name' => ''),
      'template' => 'fc-progress-next',
      'file' => 'fc_progress.blocks.inc',
    ),
  );
}
